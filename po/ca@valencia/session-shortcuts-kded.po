# Translation of session-shortcuts-kded.po to Catalan (Valencian)
# Copyright (C) 2024 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-01 01:36+0000\n"
"PO-Revision-Date: 2024-01-01 09:25+0200\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca@valencia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#: main.cpp:32
#, kde-format
msgid "Session Management"
msgstr "Gestió de sessions"

#: main.cpp:38
#, kde-format
msgid "Log Out"
msgstr "Eixida"

#: main.cpp:44
#, kde-format
msgid "Shut Down"
msgstr "Para"

#: main.cpp:50
#, kde-format
msgid "Reboot"
msgstr "Reinicia"

#: main.cpp:58
#, kde-format
msgid "Log Out Without Confirmation"
msgstr "Eixida sense confirmació"

#: main.cpp:64
#, kde-format
msgid "Shut Down Without Confirmation"
msgstr "Para sense confirmació"

#: main.cpp:70
#, kde-format
msgid "Reboot Without Confirmation"
msgstr "Torna a iniciar sense confirmació"
