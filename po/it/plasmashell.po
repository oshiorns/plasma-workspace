# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# SPDX-FileCopyrightText: 2014, 2015, 2018, 2020, 2021, 2022, 2023 Vincenzo Reale <smart2128vr@gmail.com>
#
msgid ""
msgstr ""
"Project-Id-Version: plasmashell\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-19 00:38+0000\n"
"PO-Revision-Date: 2023-11-08 09:31+0100\n"
"Last-Translator: Vincenzo Reale <smart2128vr@gmail.com>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.08.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Vincenzo Reale"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "smart2128vr@gmail.com"

#: currentcontainmentactionsmodel.cpp:206
#, kde-format
msgid "Configure Mouse Actions Plugin"
msgstr "Configura estensione delle azioni del mouse"

#: desktopview.cpp:211
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma 6.0 Dev"
msgstr "KDE Plasma 6.0 Dev"

#: desktopview.cpp:214
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma 6.0 Alpha"
msgstr "KDE Plasma 6.0 Alfa"

#: desktopview.cpp:217
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma 6.0 Beta 1"
msgstr "KDE Plasma 6.0 Beta 1"

#: desktopview.cpp:220
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma 6.0 Beta 2"
msgstr "KDE Plasma 6.0 Beta 2"

#: desktopview.cpp:223
#, kde-format
msgctxt "@label %1 is the Plasma version, RC meaning Release Candidate"
msgid "KDE Plasma 6.0 RC1"
msgstr "KDE Plasma 6.0 RC1"

#: desktopview.cpp:226
#, kde-format
msgctxt "@label %1 is the Plasma version, RC meaning Release Candidate"
msgid "KDE Plasma 6.0 RC2"
msgstr "KDE Plasma 6.0 RC2"

#: desktopview.cpp:252
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma %1 Dev"
msgstr "KDE Plasma %1 Dev"

#: desktopview.cpp:256
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma %1 Beta"
msgstr "KDE Plasma %1 Beta"

#: desktopview.cpp:259
#, kde-format
msgctxt "@label %1 is the Plasma version, %2 is the beta release number"
msgid "KDE Plasma %1 Beta %2"
msgstr "KDE Plasma %1 Beta %2"

#: desktopview.cpp:263
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma %1"
msgstr "KDE Plasma %1"

#: desktopview.cpp:269
#, kde-format
msgctxt "@info:usagetip"
msgid "Visit bugs.kde.org to report issues"
msgstr "Visita https://bugs.kde.org per segnalare errori"

#: main.cpp:88
#, kde-format
msgid "Plasma Shell"
msgstr "Plasma Shell"

#: main.cpp:100
#, kde-format
msgid "Enable QML Javascript debugger"
msgstr "Abilita il debugger Javascript QML"

#: main.cpp:103
#, kde-format
msgid "Do not restart plasma-shell automatically after a crash"
msgstr "Non riavviare plasma-shell automaticamente dopo una chiusura inattesa"

#: main.cpp:106
#, kde-format
msgid "Force loading the given shell plugin"
msgstr "Forza il caricamento di un'estensione della shell specifica"

#: main.cpp:110
#, kde-format
msgid "Replace an existing instance"
msgstr "Sostituisci un'istanza esistente"

#: main.cpp:113
#, kde-format
msgid ""
"Enables test mode and specifies the layout javascript file to set up the "
"testing environment"
msgstr ""
"Abilita la modalità di test e specifica il file javascript di struttura per "
"configurare l'ambiente di test"

#: main.cpp:114
#, kde-format
msgid "file"
msgstr "file"

#: main.cpp:118
#, kde-format
msgid "Lists the available options for user feedback"
msgstr "Elenca le opzioni disponibili per le segnalazioni degli utenti"

#: main.cpp:200
#, kde-format
msgid "Plasma Failed To Start"
msgstr "Plasma non può essere avviato"

#: main.cpp:201
#, kde-format
msgid ""
"Plasma is unable to start as it could not correctly use OpenGL 2 or software "
"fallback\n"
"Please check that your graphic drivers are set up correctly."
msgstr ""
"Plasma non è in grado di avviarsi poiché non può utilizzare correttamente "
"OpenGL 2 o il ripiego software.\n"
"Controlla che i driver grafici siano installati correttamente."

#: osd.cpp:53
#, kde-format
msgctxt "OSD informing that the system is muted, keep short"
msgid "Audio Muted"
msgstr "Audio silenziato"

#: osd.cpp:75
#, kde-format
msgctxt "OSD informing that the microphone is muted, keep short"
msgid "Microphone Muted"
msgstr "Microfono silenziato"

#: osd.cpp:91
#, kde-format
msgctxt "OSD informing that some media app is muted, eg. Amarok Muted"
msgid "%1 Muted"
msgstr "%1 silenziato"

#: osd.cpp:113
#, kde-format
msgctxt "touchpad was enabled, keep short"
msgid "Touchpad On"
msgstr "Touchpad attivo"

#: osd.cpp:115
#, kde-format
msgctxt "touchpad was disabled, keep short"
msgid "Touchpad Off"
msgstr "Touchpad disattivo"

#: osd.cpp:122
#, kde-format
msgctxt "wireless lan was enabled, keep short"
msgid "Wifi On"
msgstr "Wifi attivo"

#: osd.cpp:124
#, kde-format
msgctxt "wireless lan was disabled, keep short"
msgid "Wifi Off"
msgstr "Wifi disattivo"

#: osd.cpp:131
#, kde-format
msgctxt "Bluetooth was enabled, keep short"
msgid "Bluetooth On"
msgstr "Bluetooth attivo"

#: osd.cpp:133
#, kde-format
msgctxt "Bluetooth was disabled, keep short"
msgid "Bluetooth Off"
msgstr "Bluetooth disattivo"

#: osd.cpp:140
#, kde-format
msgctxt "mobile internet was enabled, keep short"
msgid "Mobile Internet On"
msgstr "Internet mobile attivo"

#: osd.cpp:142
#, kde-format
msgctxt "mobile internet was disabled, keep short"
msgid "Mobile Internet Off"
msgstr "Internet mobile disattivo"

#: osd.cpp:150
#, kde-format
msgctxt ""
"on screen keyboard was enabled because physical keyboard got unplugged, keep "
"short"
msgid "On-Screen Keyboard Activated"
msgstr "Tastiera su schermo attivata"

#: osd.cpp:153
#, kde-format
msgctxt ""
"on screen keyboard was disabled because physical keyboard was plugged in, "
"keep short"
msgid "On-Screen Keyboard Deactivated"
msgstr "Tastiera su schermo disattivata"

#: shellcontainmentconfig.cpp:105
#, kde-format
msgid "Internal Screen on %1"
msgstr "Schermo interno su %1"

#: shellcontainmentconfig.cpp:109
#, kde-format
msgctxt "Screen manufacturer and model on connector"
msgid "%1 %2 on %3"
msgstr "%1 %2 su %3"

#: shellcontainmentconfig.cpp:132
#, kde-format
msgid "Disconnected Screen %1"
msgstr "Schermo disconnesso %1"

#: shellcorona.cpp:198 shellcorona.cpp:200
#, kde-format
msgid "Show Desktop"
msgstr "Mostra il desktop"

#: shellcorona.cpp:200
#, kde-format
msgid "Hide Desktop"
msgstr "Nascondi il desktop"

#: shellcorona.cpp:216
#, kde-format
msgid "Show Activity Switcher"
msgstr "Mostra il selettore delle attività"

#: shellcorona.cpp:227
#, kde-format
msgid "Stop Current Activity"
msgstr "Ferma l'attività attuale"

#: shellcorona.cpp:235
#, kde-format
msgid "Switch to Previous Activity"
msgstr "Passa all'attività precedente"

#: shellcorona.cpp:243
#, kde-format
msgid "Switch to Next Activity"
msgstr "Passa all'attività successiva"

#: shellcorona.cpp:258
#, kde-format
msgid "Activate Task Manager Entry %1"
msgstr "Attiva voce %1 del gestore dei processi"

#: shellcorona.cpp:285
#, kde-format
msgid "Manage Desktops And Panels..."
msgstr "Gestisci desktop e pannelli..."

#: shellcorona.cpp:307
#, kde-format
msgid "Move keyboard focus between panels"
msgstr "Sposta il fuoco della tastiera tra i pannelli"

#: shellcorona.cpp:2047
#, kde-format
msgid "Add Panel"
msgstr "Aggiungi pannello"

#: shellcorona.cpp:2089
#, kde-format
msgctxt "Creates an empty containment (%1 is the containment name)"
msgid "Empty %1"
msgstr "%1 vuoto"

#: softwarerendernotifier.cpp:27
#, kde-format
msgid "Software Renderer In Use"
msgstr "Resa software in uso"

#: softwarerendernotifier.cpp:28
#, kde-format
msgctxt "Tooltip telling user their GL drivers are broken"
msgid "Software Renderer In Use"
msgstr "Resa software in uso"

#: softwarerendernotifier.cpp:29
#, kde-format
msgctxt "Tooltip telling user their GL drivers are broken"
msgid "Rendering may be degraded"
msgstr "La resa potrebbe essere degradata"

#: softwarerendernotifier.cpp:39
#, kde-format
msgid "Never show again"
msgstr "Non mostrare più"

#: userfeedback.cpp:36
#, kde-format
msgid "Panel Count"
msgstr "Conteggio pannelli"

#: userfeedback.cpp:40
#, kde-format
msgid "Counts the panels"
msgstr "Conteggio dei pannelli"

#~ msgctxt "@label %1 version string"
#~ msgid "Plasma %1 Development Build"
#~ msgstr "Versione di sviluppo di Plasma %1"

#~ msgid ""
#~ "Load plasmashell as a standalone application, needs the shell-plugin "
#~ "option to be specified"
#~ msgstr ""
#~ "Carica plasmashell come applicazione autonoma, richiede che l'opzione "
#~ "plasma-shell sia specificata"

#~ msgid "Unknown %1"
#~ msgstr "%1 sconosciuto"

#~ msgid "Unable to load script file: %1"
#~ msgstr "Impossibile caricare il file di script: %1"

#~ msgid "Activities..."
#~ msgstr "Attività..."

#~ msgctxt "Fatal error message body"
#~ msgid ""
#~ "All shell packages missing.\n"
#~ "This is an installation issue, please contact your distribution"
#~ msgstr ""
#~ "Mancano tutti i pacchetti della shell.\n"
#~ "Questo è un problema di installazione, contatta la tua distribuzione"

#~ msgctxt "Fatal error message title"
#~ msgid "Plasma Cannot Start"
#~ msgstr "Plasma non può essere avviato"

#~ msgctxt "Fatal error message body"
#~ msgid "Shell package %1 cannot be found"
#~ msgstr "Il pacchetto della shell %1 non può essere trovato"

#~ msgid "Main Script File"
#~ msgstr "File di script principale"

#~ msgid "Force a windowed view for testing purposes"
#~ msgstr "Forza una vista con finestra per scopi di test"

#~ msgid "Deprecated, does nothing"
#~ msgstr "Obsoleto, non fa nulla"

#~ msgid ""
#~ "Your graphics hardware does not support OpenGL (ES) 2. Plasma will abort "
#~ "now."
#~ msgstr ""
#~ "La tua scheda grafica non supporta OpenGL (ES) 2. Plasma sarà terminato."

#~ msgid "Incompatible OpenGL version detected"
#~ msgstr "Versione OpenGL rilevata non compatibile"

#~ msgid "Recent number of crashes"
#~ msgstr "Numero di chiusure inattese recenti"

#~ msgid "Shuts up the output"
#~ msgstr "Silenzia l'uscita"
