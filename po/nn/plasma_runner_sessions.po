# Translation of plasma_runner_sessions to Norwegian Nynorsk
#
# Karl Ove Hufthammer <karl@huftis.org>, 2008, 2009, 2018, 2020, 2022, 2023.
# Eirik U. Birkeland <eirbir@gmail.com>, 2010, 2011.
msgid ""
msgstr ""
"Project-Id-Version: krunner_sessions\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-06 01:40+0000\n"
"PO-Revision-Date: 2023-02-23 17:59+0100\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: sessionrunner.cpp:20
#, kde-format
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to log out of the "
"session"
msgid "logout;log out"
msgstr "logg ut;utlogging"

#: sessionrunner.cpp:23
#, kde-format
msgid "Logs out, exiting the current desktop session"
msgstr "Loggar ut av økta som køyrer"

#: sessionrunner.cpp:26
#, fuzzy, kde-format
#| msgctxt ""
#| "KRunner keywords (split by semicolons without whitespace) to shut down "
#| "the computer"
#| msgid "shutdown;shut down"
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to shut down the "
"computer"
msgid "shutdown;shut down;power;power off"
msgstr "slå av;avslåing;nedstenging;steng ned"

#: sessionrunner.cpp:29
#, kde-format
msgid "Turns off the computer"
msgstr "Slår av datamaskina"

#: sessionrunner.cpp:32
#, kde-format
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to restart the "
"computer"
msgid "restart;reboot"
msgstr "omstart;start om;start på nytt;restart"

#: sessionrunner.cpp:35
#, kde-format
msgid "Reboots the computer"
msgstr "Startar datamaskina på nytt"

#: sessionrunner.cpp:39
#, kde-format
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to lock the screen"
msgid "lock;lock screen"
msgstr "lås;lås skjermen;skjemlås"

#: sessionrunner.cpp:41
#, kde-format
msgid "Locks the current sessions and starts the screen saver"
msgstr "Låser økta og startar pauseskjermen"

#: sessionrunner.cpp:44
#, kde-format
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to save the "
"desktop session"
msgid "save;save session"
msgstr "lagra;lagra økt;øktlagring"

#: sessionrunner.cpp:47
#, kde-format
msgid "Saves the current session for session restoration"
msgstr "Lagra gjeldande økt for seinare gjenoppretting"

#: sessionrunner.cpp:50
#, kde-format
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to switch user "
"sessions"
msgid "switch user;new session"
msgstr "byt brukar;ny økt;brukarbyte"

#: sessionrunner.cpp:53
#, kde-format
msgid "Starts a new session as a different user"
msgstr "Startar ei ny økt som ein annan brukar"

#: sessionrunner.cpp:56
#, kde-format
msgctxt "KRunner keyword to list user sessions"
msgid "sessions"
msgstr "økter"

#: sessionrunner.cpp:57
#, kde-format
msgid "Lists all sessions"
msgstr "Vis alle økter"

#: sessionrunner.cpp:60
#, kde-format
msgctxt "KRunner keyword to switch user sessions"
msgid "switch"
msgstr "byt"

#: sessionrunner.cpp:62
#, kde-format
msgid ""
"Switches to the active session for the user :q:, or lists all active "
"sessions if :q: is not provided"
msgstr ""
"Byter til den aktive økta for brukaren :q:, eller viser alle dei aktive "
"øktene om :q: ikkje er med"

#: sessionrunner.cpp:81
#, kde-format
msgctxt "log out command"
msgid "Log Out"
msgstr "Logg ut"

#: sessionrunner.cpp:89
#, kde-format
msgctxt "turn off computer command"
msgid "Shut Down"
msgstr "Slå av"

#: sessionrunner.cpp:96
#, kde-format
msgctxt "restart computer command"
msgid "Restart"
msgstr "Start på nytt"

#: sessionrunner.cpp:103
#, kde-format
msgctxt "lock screen command"
msgid "Lock"
msgstr "Lås"

#: sessionrunner.cpp:110
#, kde-format
msgid "Save Session"
msgstr "Lagra økta"

#: sessionrunner.cpp:151
#, kde-format
msgid "Switch User"
msgstr "Byt brukar"

#: sessionrunner.cpp:237
#, kde-format
msgid ""
"<p>You are about to enter a new desktop session.</p><p>A login screen will "
"be displayed and the current session will be hidden.</p><p>You can switch "
"between desktop sessions using:</p><ul><li>Ctrl+Alt+F{number of session}</"
"li><li>Plasma search (type '%1')</li><li>Plasma widgets (such as the "
"application launcher)</li></ul>"
msgstr ""
"<p>Du er ferd med å opna ei ny skrivebordsøkt.</p><p>Gjeldande økt vert "
"gøymd, og du får presentert ein innloggingsvindauge.</p><p>Slik byter du "
"mellom økter som køyrer:</p><ul><li>Ctrl + Alt +F{øktnummer}</li><li>Plasma-"
"søk (skriv «%1»)</li><li>Plasma-element (for eksempel programstartaren)</"
"li></ul>"

#: sessionrunner.cpp:246
#, kde-format
msgid "New Desktop Session"
msgstr "Ny skrivebordsøkt"
